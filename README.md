# Kapamilya Accounts LOGIN Enhancement

## Setup

#### Requirements
* NodeJS (https://nodejs.org/en/)
* Gulp (http://gulpjs.com/)
* VueJS (https://vuejs.org/)

To make sure you have Node, npm, and gulp installed, run three simple commands to see what version of each is installed:
* To see if Node is installed, type `node -v` in Terminal. This should print the version number.
* To see if NPM is installed, type `npm -v` in Terminal. This should print the version number.
* To see if gulp is installed, type `gulp -v` in Terminal. This should print the version number.

#### Installation
1. **Run npm for one-time installation of development dependencies**  
  `sudo npm install`

2. **Refer to the link below for vuejs turorials**
  https://vuejs.org/v2/guide/index.html
  https://medium.com/codingthesmartway-com-blog/vue-js-2-quickstart-tutorial-2017-246195cfbdd2

3. **Run gulp to compile and watch**  
  `npm start` or `npm run serve` - depend on how you set it up in your package.json file

## Development
```
$ Creating new pages/tabs - add a new file into `/src/views/` folder with the extension of .vue
$ Note: After you created a new page you should add/setup your pages into routes you will find it in `src/router.js`
``` 
## Store
```
`/src/store/`
$ `/src/store/index.js`
* This is where you can store all data coming from gigya accounts. 
$ `/src/store/vars.js`
* This is the variables declaration in store.
```
## Dependencies
```
* vee-select for dropdown
* lodash.js to manipulate object data.
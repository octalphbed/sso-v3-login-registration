import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import store from './store';
import Vuelidate from 'vuelidate';
import VueSSOPlugin from './plugins/sso';
import VueCustomElement from 'vue-custom-element';
import { initRouter } from './router';
import { sync } from 'vuex-router-sync';

import './assets/css/master.scss';
import './assets/css/animate.css';
import 'document-register-element/build/document-register-element';
 
Vue.use(Vuelidate);
Vue.use(VueSSOPlugin);
Vue.use(VueCustomElement);

// disable production console log tip
Vue.config.productionTip = false;

// set axios default base api url
const initAxios = (router) => {
    axios.defaults.baseURL = process.env.VUE_APP_API_MGR;
    
    axios.interceptors.response.use(response => {
        // Do something with response data
        let sCode = response.data.statusCode;
        let responseURL = response.request.responseURL ? response.request.responseURL : response.config.url;

        let gigyaErrors = [400124,500028,500026,500001,504001];
            
        if (responseURL.includes("sso")){
            if (gigyaErrors.includes(sCode)){
                // Error redirect to page
                router.replace({name: 'rate-limit'});
            }
        }
        
        return response;
    });
}

// set $http value to axios as default
Vue.prototype.$http = axios;

// gigya js sdk config
window.__gigyaConf = {
    enableSSOToken: true
};

export const ssoInit = (routerMode) => {
    const router = initRouter(routerMode);

    initAxios(router);
    sync(store, router);
    
    App.store = store;
    App.router = router;
    Vue.customElement('sso-accounts', App);
};
import { isArray } from "lodash";

const HidePage_LocalStorage = "ssoaccounts:hide-pages";

const getHidePages = () => {
  let pages = JSON.parse(localStorage.getItem(HidePage_LocalStorage));

  if (!isArray(pages)) {
    pages = [];
  }

  return pages;
};

export { HidePage_LocalStorage, getHidePages };

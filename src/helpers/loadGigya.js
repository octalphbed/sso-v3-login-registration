const loadGigyaWebSDK = (apiKey, callback) => {
  const existingScript = document.getElementById("GigyaJS");

  if (!existingScript) {
    const script = document.createElement("script");
    script.src = "https://cdns.gigya.com/js/gigya.js?apiKey=" + apiKey;
    script.id = "GigyaJS";

    document.body.appendChild(script);
    script.onload = () => {
      if (callback) callback();
    };
  }
};

export { loadGigyaWebSDK };

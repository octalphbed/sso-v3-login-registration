'use strict';

import Uninav from '../components/Uninav.vue';
import Page from '../components/Page.vue';
import Input from '../components/Input.vue';
import Button from '../components/Button.vue';
import Select from '../components/Select.vue';
import PinInput from '../components/PinInput.vue';
import Checkbox from '../components/Checkbox.vue';
import Salutation from '../components/Salutation.vue';
import Birthdate from '../components/Birthdate';
import Tooltip from '../components/Tooltip.vue';
import Loader from '../components/Loader.vue';
import Sites from '../components/Sites.vue';
import MobileInput from '../components/MobileInput.vue';
import OverviewHeader from '../components/OverviewHeader.vue';
import PasswordStrength from '../components/PasswordStrength.vue';
import ConsentTooltip from '../components/ConsentTooltip.vue';

import { setCookie, getCookie } from 'tiny-cookie';
import { loadGigyaWebSDK } from '../helpers/loadGigya';
import { loadFacebookSDK } from '../helpers/loadFacebook';

const SSOPlugin = {
	ssoApp: null,
	activePage: null,
	installed: false,

	install(Vue) {
		if (SSOPlugin.installed) {
			return;
		}

		const ssoGlobals = {};

		Vue.prototype.$loader = {
			_setPage(page) {
				SSOPlugin.activePage = page;
			},

			show(params) {
				if (SSOPlugin.activePage) {
					SSOPlugin.activePage.showLoader(params);
				}
			},

			hide() {
				if (SSOPlugin.activePage) {
					SSOPlugin.activePage.hideLoader();
				}
			}
		};

		Vue.prototype.$sso = {
			_setInstance(app, callback) {
				SSOPlugin.ssoApp = app;

				if (!ssoGlobals._config) {
					Object.defineProperties(ssoGlobals, {
						_config: {
							writable: false,
							value: {}
						},
						show: {
							writable: false,
							value: function(params = {}) {
								SSOPlugin.ssoApp.toggleOpen(true, params);
							}
						},
						hide: {
							writable: false,
							value: function() {
								SSOPlugin.ssoApp.toggleOpen(false, null);
							}
						},
						addListener: {
							writable: false,
							value: function(event, callback) {
								SSOPlugin.ssoApp.$store.commit('ADD_LISTENERS', {
									event,
									callback
								});
							}
						}
					});
				}

				if (!window.SSO) {
					Object.defineProperty(window, 'SSO', {
						writable: false,
						value: ssoGlobals
					});

					window.addEventListener('click', function(e) {
						const uninavLogin = document.getElementById('uninav-login');
						const ssoContainer = document.getElementById('sso-main');

						if (
							uninavLogin &&
							!uninavLogin.contains(e.target) &&
							(ssoContainer && !ssoContainer.contains(e.target))
						) {
							if (SSOPlugin.ssoApp.isOpen) {
								ssoGlobals.hide();
							}
						}
					});
				}

				callback(window.sso);
			},

			setApiKey(apiKey, callback) {
				// load gigya js sdk to body
				loadGigyaWebSDK(apiKey, () => {
					Object.defineProperty(window, 'onGigyaServiceReady', {
						writable: false,
						value: function() {
							if (window.gigya) {
								callback(window.gigya);
							}
						}
					});
				});

				// load Facebook SDK
				loadFacebookSDK(() => {
					window.FB.init({
						appId: process.env.VUE_APP_FB_APPID,
						cookie: false, // enable cookies to allow the server to access
						xfbml: true, // parse social plugins on this page
						version: 'v2.8', // use graph api version 2.8
						channelUrl: '../../public/channel.html'
					});
				});

				if (!ssoGlobals._config) {
					Object.defineProperty(ssoGlobals._config, 'apiKey', {
						writable: false,
						value: apiKey
					});
				}
			},

			show(params = {}) {
				if (SSOPlugin.ssoApp) {
					SSOPlugin.ssoApp.toggleOpen(true, params);
				}
			},

			hide() {
				if (SSOPlugin.ssoApp) {
					SSOPlugin.ssoApp.toggleOpen(false);
				}
			},

			successLogin({ cookieName, cookieValue, params = {} }) {
				if (typeof params.query === 'undefined') {
					params.query = {};
				}

				if (
					typeof params.path === 'undefined' ||
					typeof params.path !== 'string'
				) {
					params.path = '/welcome';
				}

				const gigya = window.gigya;
				const uninav = window.uninav;
				const store = SSOPlugin.ssoApp.$store;
				const router = SSOPlugin.ssoApp.$router;

				// store gigya session info from the cookie
				setCookie(cookieName, cookieValue);

				// call gigya accounts.getAccountInfo to get current loggedin user
				gigya.accounts.getAccountInfo({
					include: 'loginIDs, emails,  profile, data',
					callback: function(response) {
						// if status OK, navigate to welcome page
						if (response.status === 'OK') {
							// call all onLogin event listeners.
							store.dispatch('INVOKE_LISTENERS', {
								eventName: 'onLogin',
								payload: response
							});

							// uninav js
							if (uninav && uninav._setLoginState) {
								uninav._setLoginState(response);
							}

							// store user to our store manager
							store.commit('SET_LOGGEDIN_USER', response);
							
							// RAYGUN SET USER
							rg4js('setUser', {
								identifier: response.UID,
								isAnonymous: false,
								email: response.loginIDs.username,
								firstName: response.profile.firstName,
								fullName: response.profile.firstName + ' ' + response.profile.lastName
						  	});


							// navigate to page
							router.replace({ path: params.path, query: params.query });
						}

						// else, navigate to our error handler page
						else {
							let error = 'ERROR :(';
							let message = response.errorMessage;

							switch (response.errorCode) {
								case 403048:
									message = 'Limit of request has been reached. Please try again after a minute.';
									break;
							}

							router.push({
								path: '/error',
								query: Object.assign({}, { error, message }, params.query)
							});
						}
					}
				});
			}
		};

		if (process.env.VUE_APP_LOCAL_MODE) {
			if (!getCookie('app_siteurl'))
				setCookie('app_siteurl', 'http://qa-thankyou.abs-cbn.com');

			if (!getCookie('.ka_redirect'))
				setCookie('.ka_redirect', 'https://thankyou.abs-cbn.com');
		}

		Vue.component(Uninav.name, Uninav);
		Vue.component(Page.name, Page);
		Vue.component(Input.name, Input);
		Vue.component(Button.name, Button);
		Vue.component(Checkbox.name, Checkbox);
		Vue.component(Salutation.name, Salutation);
		Vue.component(Birthdate.name, Birthdate);
		Vue.component(PinInput.name, PinInput);
		Vue.component(Select.name, Select);
		Vue.component(Tooltip.name, Tooltip);
		Vue.component(Loader.name, Loader);
		Vue.component(Sites.name, Sites);
		Vue.component(MobileInput.name, MobileInput);
		Vue.component(OverviewHeader.name, OverviewHeader);
		Vue.component(PasswordStrength.name, PasswordStrength);
		Vue.component(ConsentTooltip.name, ConsentTooltip);

		SSOPlugin.installed = true;
	}
};

export default SSOPlugin;

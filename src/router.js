import Vue from 'vue';
import store from './store';
import Router from 'vue-router';
import { find } from 'lodash';

Vue.use(Router);

export const initRouter = (mode) => {

	const router = new Router({
		mode: mode,

		routes: [
			// CUSTOM REDIRECTS
			{ path: '*', redirect: '/404' },
			{
				path: '/404',
				name: 'PageNotFound',
				component: () => import('./views/PageNotFound.vue')
			},
			{
				path: '/',
				redirect: '/signin'
			},
			{
				path: '/reset-password',
				redirect: '/forgot/password-reset'
			},
			{
				path: '/default',
				name: 'default',
				component: () => import('./views/Home.vue')
			},
			{
				path: '/welcome',
				name: 'welcome',
				component: () => import('./views/Welcome.vue')
			},
			{
				path: '/thank-you',
				name: 'thank-you',
				component: () => import('./views/ThankYou.vue')
			},
			{
				path: '/redirecting',
				name: 'site-redirect',
				component: () => import('./views/Redirect.vue')
			},

			{
				path: '/signin',
				name: 'signin',
				component: () => import('./views/SignIn.vue'),
				meta: { previousRoute: '/default' }
			},
			{
				path: '/confirm-email/link',
				name: 'confirm-email-link',
				component: () => import('./views/ConfirmEmail/ConfirmLink.vue')
			},
			{
				path: '/confirm-secondary-email',
				name: 'confirm-secondary-email',
				component: () => import('./views/ConfirmEmail/ConfirmSecondaryEmail.vue')
			},
			{
				path: '/verify-email',
				name: 'verify-email',
				component: () => import('./views/ConfirmEmail/VerifyEmail.vue')
			},
			{
				path: '/mobile-verify',
				name: 'mobile-verify',
				component: () => import('./views/MobileVerify.vue')
			},
			{
				path: '/kapamilyaname-completion',
				name: 'kapamilyaname-completion',
				component: () => import('./views/ProfileCompletion/KapamilyaName.vue'),
				meta : { previousRoute: '/signin'}
			},
			{
				path: '/rate-limit',
				name: 'rate-limit',
				component: () => import('./views/RateLimit.vue'),
			},
			
			// SIGNUP WITH EMAIL, MOBILE OR FACEBOOK
			{
				path: '/signup',
				component: () => import('./views/Signup/index.vue'),
				children: [
					{
						path: '/',
						name: 'signup',
						component: () => import('./views/Signup/Signup.vue'),
						meta: { previousRoute: '/signin' }
					},
					{
						path: 'email',
						name: 'signup-email',
						component: () => import('./views/Signup/Email.vue'),
						meta: { previousRoute: '/signup' }
					},
					{
						path: 'mobile',
						component: () => import('./views/Signup/Mobile/Index.vue'),
						children: [
							{
								path: '/',
								name: 'signup-mobile',
								component: () => import('./views/Signup/Mobile/Mobile.vue'),
								meta: { previousRoute: '/signup' }
							},
							{
								path: 'verification',
								name: 'mobile-churn',
								component: () => import('./views/Signup/Mobile/Churning.vue'),
								meta: { 
									previousRoute: {
										text: 'Back to login',
										path: '/signin'
									}
								}
							}
						]
					}
				]
			},

			// FORGOT KAPAMILYA NAME OR PASSOWRD
			{
				path: '/forgot',
				name: 'forgot',
				component: () => import('./views/Forgot/Index.vue'),
				children: [
					// FORGOT KAPAMILYA NAME
					{
						path: 'kapamilya-name',
						component: () => import('./views/Forgot/KapamilyaName/Index.vue'),
						meta: { previousRoute: '/signin' },
					},
					{
						name: 'forgot-kn-email-sent',
						path: 'kapamilya-name/email-sent',
						component: () => import('./views/Forgot/KapamilyaName/EmailSent.vue'),
						meta: { previousRoute: '/forgot/kapamilya-name' }
					},
					{
						path: 'kapamilya-name/message-sent',
						component: () => import('./views/Forgot/KapamilyaName/MessageSent.vue'),
						meta: { previousRoute: '/forgot/kapamilya-name' }
					},
					// ----------------------------------------
					
					// FORGOT PASSWORD
					{
						path: 'password',
						name: 'forgot-password',
						component: () => import('./views/Forgot/Password/Index.vue'),
						meta: { previousRoute: '/signin' },
					},
					{
						path: 'password/email-sent',
						component: () => import('./views/Forgot/Password/EmailSent.vue'),
						meta: { previousRoute: '/forgot/password' }
					},
					{
						path: 'password-reset',
						component: () => import('./views/Forgot/Password/Change.vue')
					},
					{
						path: 'password-reset/success',
						name: 'password-reset-success',
						component: () => import('./views/Forgot/Password/Success.vue')
					}
					// ----------------------------------------
				]
			},

			// SOCIAL LOGIN
			{
				path: '/social',
				component: () => import('./views/SocialLogin/Index.vue'),
				children: [
					{ 
						path: 'account-linking',
						name: 'social-account-linking',
						component: () => import('./views/SocialLogin/AccountLinking.vue')
					},
					{
						path: 'email-declined',
						name: 'social-email-declined',
						component: () => import('./views/SocialLogin/EmailDeclined.vue')
					}
				]
			},

			// MOBILE LOGIN
			{
				path: '/mobile',
				component: () => import('./views/MobileLogin/Index.vue'),
				children: [
					{
						path: '/',
						redirect: { name: 'mobile-login' }
					},
					{
						path: 'login',
						name: 'mobile-login',
						component: () => import('./views/MobileLogin/MobileLogin.vue'),
						meta: { previousRoute: '/signin' }
					},
					{
						path: 'link-account',
						name: 'mobile-link-account',
						component: () => import('./views/MobileLogin/AccountLinking.vue'),
						meta: { previousRoute: '/mobile' }
					},
					{
						path: 'not-allowed',
						name: 'mobile-not-allowed',
						component: () => import('./views/MobileLogin/NotAllowed.vue'),
						meta: { previousRoute: '/mobile' }
					}
				]
			},


			// Profile Completion
			{
				path: '/profile-completion',
				component: () => import('./views/ProfileCompletion/Index.vue'),
				children: [
					{
						path: '/',
						name: 'profile-completion-default',
						component: () => import('./views/ProfileCompletion/Default.vue')
					},
					{
						path: 'social',
						name: 'profile-completion-social',
						component: () => import('./views/ProfileCompletion/Social.vue')
					},
					{
						path: 'mobile',
						name: 'profile-completion-mobile',
						component: () => import('./views/ProfileCompletion/Mobile.vue')
					},
					{
						path: 'name',
						name: 'name-completion',
						component: () => import('./views/ProfileCompletion/Name.vue')
					}
				]
			},

			// Resend Verification
			{
				path: '/resend-verification/email',
				name: 'resend-verification-email',
				component: () => import('./views/ResendVerification/Email.vue')
			},

			// Overview
			{
				name: 'overview',
				path: '/overview',
				component: () => import('./views/Overview/Overview.vue')
			},
			
			// Privacy Notice
			{
				name: 'privacy-policy',
				path: '/privacy-policy',
				component: () => import('./views/Overview/PrivacyNotice.vue')
			},

			// Terms And Conditions
			{
				name: 'terms-and-conditions',
				path: '/terms-and-conditions',
				component: () => import('./views/Overview/TermsAndConditions.vue')
			},

			// Privacy Policy Global
			{
				name: 'privacy-policy-global',
				path: '/privacy-policy-global',
				component: () => import('./views/Overview/Global/PrivacyPolicyGlobal.vue')
			},

			// Terms And Condition Global
			{
				name: 'terms-and-condition-global',
				path: '/terms-and-condition-global',
				component: () => import('./views/Overview/Global/TermsAndConditionsGlobal.vue')
			},

			// Subsidiaries 
			{
				name: 'subsidiaries',
				path: '/subsidiaries',
				component: () => import('./views/Overview/Subsidiary.vue')
			},

			// On Boarding Verify Email
			{
				name: 'ob-verify-email',
				path: '/on-boarding/verify-email',
				component: () => import('./views/OnBoarding/VerifyEmail.vue')
			},

			// On Boarding Expired Link
			{
				name: 'ob-expired-link',
				path: '/on-boarding/expired-link',
				component: () => import('./views/OnBoarding/ExpiredLink.vue')
			},
			

		]
	});

	router.beforeEach((to, from, next) => {
		const hiddenPages = store.state.hiddenPages;
		const hiddenPage = find(hiddenPages, function(_page) {
			return _page.from === to.path;
		});
	
		// prevent a page from showing if 'dont show' tick box is on.
		if (hiddenPage) {
			if (hiddenPage.from === '/welcome') {
				if (to.query.from === 'signup') {
					next(hiddenPage.to);
					return;
				} else {
					store.dispatch('GET_LOGGEDIN_USER', window.gigya).then(() => {
						window.SSO.hide();
					});
	
					next({ path: '/redirecting' });
					return;
				}
			}
	
			next(hiddenPage.to);
			return;
		}
	
		next();
	});

	return router;
}



import axios from 'axios';
import { merge } from 'lodash';

const SSO_CMS_BASE_URL = process.env.VUE_APP_SSO_CMS_API;
const CMS_BASE_URL = process.env.VUE_APP_CMS_API
    
export const CMS_API = {
    POST: (endpoint, options = {}) => {
        axios({
            method: 'POST',
            baseURL : options.baseURL,
            url: `/api/cms/${endpoint}`,
            data: options.data,
            params: options.params
        })
            .then(response => {
                if (options.callback && typeof options.callback === 'function') {
                    options.callback(response.data);
                }
            })
            .catch(() => {
                if (options.callback && typeof options.callback === 'function') {
                    options.callback({
                        errorMessage: 'An error occurred sending the request'
                    });
                }
            });
    },

    GET: (apiName, options = {}) => {
        axios({
            method: 'GET',
            baseURL : options.baseURL,
            url: `/api/cms/${apiName}`,
            data: options.data,
            params: options.params
        })
            .then(response => {
                if (options.callback && typeof options.callback === 'function') {
                    options.callback(response.data);
                }
            })
            .catch(() => {
                if (options.callback && typeof options.callback === 'function') {
                    if (options.failedResponse) {
                        options.callback(options.failedResponse);
                    } else {
                        options.callback({
                            errorMessage: 'An error occurred sending the request'
                        });
                    }
                }
            });
    }
};

export const CMS_ACCOUNT_API = {
    POST: (endpoint, options = {}) => {
        axios({
            method: 'POST',
            baseURL : CMS_BASE_URL,
            url: `/api/cms/${endpoint}`,
            data: options.data,
            params: options.params
        })
            .then(response => {
                if (options.callback && typeof options.callback === 'function') {
                    options.callback(response.data);
                }
            })
            .catch(() => {
                if (options.callback && typeof options.callback === 'function') {
                    options.callback({
                        errorMessage: 'An error occurred sending the request'
                    });
                }
            });
    },

    GET: (apiName, options = {}) => {
        axios({
            method: 'GET',
            baseURL : CMS_BASE_URL,
            url: `/api/cms/${apiName}`,
            data: options.data,
            params: options.params
        })
            .then(response => {
                if (options.callback && typeof options.callback === 'function') {
                    options.callback(response.data);
                }
            })
            .catch(() => {
                if (options.callback && typeof options.callback === 'function') {
                    if (options.failedResponse) {
                        options.callback(options.failedResponse);
                    } else {
                        options.callback({
                            errorMessage: 'An error occurred sending the request'
                        });
                    }
                }
            });
    }
}


export const CMS_GET_CONTENT = options => {
	CMS_API.GET('GetSSOContent', merge(options, { baseURL: SSO_CMS_BASE_URL }));
}

export const CMS_GET_GLOBAL_PRIVACY_POLICY = options => {
	CMS_API.GET('GlobalPrivacy', merge(options, { baseURL: SSO_CMS_BASE_URL }));
}

export const CMS_GET_GLOBAL_TERMS = options => {
	CMS_API.GET('GlobalTerms', merge(options,{ baseURL: SSO_CMS_BASE_URL }));
}
export const CMS_GET_SUBSIDIARIES = options => {
    CMS_API.GET('Subsidiary', merge(options,{ baseURL: SSO_CMS_BASE_URL }));
}

export const CMS_GET_SSOSETTINGS = options => {
    CMS_ACCOUNT_API.GET('SSOSettings', merge(options,{ baseURL: CMS_BASE_URL }));
}

export const CMS_GET_PrivacyActiveVersion = options => {
	CMS_ACCOUNT_API.GET('PrivacyActiveVersion',  merge(options,{ baseURL: CMS_BASE_URL }));
}

export const CMS_GET_TermsActiveVersion = options => {
	CMS_ACCOUNT_API.GET('TermsActiveVersion',  merge(options,{ baseURL: CMS_BASE_URL }));
}

export const CMS_GET_MarketingConsent= options => {
	CMS_ACCOUNT_API.GET('MarketingConsentActiveVersion',  merge(options,{ baseURL: CMS_BASE_URL }));
}
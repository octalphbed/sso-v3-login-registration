import { find, isEmpty, assign } from 'lodash';

export const FB_STATUS = {
	EMAIL: 0,
	MOBILE: 1,
	DECLINED_PERMISSION: 2,
	FAILED: 3
};

const defaultParams = {
	scope: 'email, public_profile',
	return_scopes: true
};

const fbLogin = (params, callback) => {
	if (!window.FB) {
		callback({ status: FB_STATUS.FAILED });
		return;
	}

	window.FB.login(({ authResponse, status }) => {
		if (status !== 'connected') {
			callback({ status: FB_STATUS.FAILED });
			return;
		}

		getMe(result => {
			callback(
				assign({}, result, {
					access_token: authResponse.accessToken,
					user_id: authResponse.userID
				})
			);
		});
	}, params);
};

const getMe = callback => {
	if (!window.FB) {
		callback({ status: FB_STATUS.FAILED });
		return;
	}

	window.FB.api(
		'/me',
		{ fields: 'id, email, first_name, last_name' },
		({ id, email, first_name, last_name }) => {
			// check user email address is included in the response
			if (typeof email === 'undefined') {
				// check what permissions the user has declined.
				checkGrantedPermissions(id, status => {
					// if user email address was declined
					if (status === FB_STATUS.DECLINED_PERMISSION) {
						// ask you user that he/she must grant us to get his/her email address from facebook.
						callback({ status: status, message: 'email declined' });
					}

					// posible that the user facebook account was created using his/her mobile number.
					else if (status === FB_STATUS.MOBILE) {
						callback({ 
							ln: last_name,
							fn: first_name,
							status: status,
							message: 'mobile registered' 
						});
					} else {
						callback({ status: status });
					}
				});
			}

			// if user email address was included in the response
			else {
				callback({
					email: email,
					ln: last_name,
					fn: first_name,
					status: FB_STATUS.EMAIL,
					message: 'email registered'
				});
			}
		}
	);
};

const checkGrantedPermissions = (userID, callback) => {
	if (!window.FB) {
		callback(FB_STATUS.FAILED);
		return;
	}

	window.FB.api(`/${userID}/permissions`, ({ data }) => {
		if (isEmpty(data)) {
			callback(FB_STATUS.FAILED);
			return;
		}

		const emailDeclined = find(data, function(p) {
			return p.permission === 'email' && p.status === 'declined';
		});

		if (isEmpty(emailDeclined)) {
			callback(FB_STATUS.MOBILE);
		} else {
			callback(FB_STATUS.DECLINED_PERMISSION);
		}
	});
};

export const FacebookLogin = (callback, params) => fbLogin(assign({}, defaultParams, params), callback);

import axios from "axios";
export const getClientIP = callback => {
  axios.get('//ipinfo.io/geo').then(response => {
    callback(response.data);
  });
};

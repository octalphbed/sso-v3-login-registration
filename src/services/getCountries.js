import axios from 'axios';
export default callback => {
	
	const BASE_URL = process.env.VUE_APP_CMS_API;

	axios({ baseURL: BASE_URL, url: '/api/cms/GetCountries' })
		.then(res => {
			callback(res.data);
		})
		.catch(() => {
			callback([]);
		});
};

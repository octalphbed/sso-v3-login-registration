import axios from "axios";
import store from "../store";
import { merge } from "lodash";

const DEFAULT_HEADERS = () => {
  return {
    headers: {
      "Verification-Mode": store.getters.verifType
    }
  };
};

const URL_PREFIX = process.env.NODE_ENV === "production" ? "/sso/api" : "/api";

export const SSO_API = {
  POST: (endpoint, options = {}) => {
    axios({
      method: "POST",
      url: `${URL_PREFIX}/${endpoint}`,
      data: options.data,
      params: options.params,
      headers: merge({}, options.headers, {
        "Ocp-Apim-Subscription-Key": store.state.ocpKey,
        "SSO-Native-Origin": window.location.origin
      })
    })
      .then(response => {
        if (options.callback && typeof options.callback === "function") {
          options.callback(response.data);
        }
      })
      .catch(() => {
        if (options.callback && typeof options.callback === "function") {
          options.callback({
            errorMessage: "An error occurred sending the request"
          });
        }
      });
  },

  GET: (apiName, options = {}) => {
    axios({
      method: "GET",
      url: `${URL_PREFIX}/${apiName}`,
      data: options.data,
      params: options.params,
      headers: merge({}, options.headers, {
        "Ocp-Apim-Subscription-Key": store.state.ocpKey,
        "SSO-Native-Origin": window.location.origin
      })
    })
      .then(response => {
        if (options.callback && typeof options.callback === "function") {
          options.callback(response.data);
        }
      })
      .catch(() => {
        if (options.callback && typeof options.callback === "function") {
          if (options.failedResponse) {
            options.callback(options.failedResponse);
          } else {
            options.callback({
              errorMessage: "An error occurred sending the request"
            });
          }
        }
      });
  },
  PUT: (apiName, options = {}) => {
    axios({
      method: "PUT",
      url: `${URL_PREFIX}/${apiName}`,
      data: options.data,
      params: options.params,
      headers: merge({}, options.headers, {
        "Ocp-Apim-Subscription-Key": store.state.ocpKey,
        "SSO-Native-Origin": window.location.origin
      })
    })
      .then(response => {
        if (options.callback && typeof options.callback === "function") {
          options.callback(response.data);
        }
      })
      .catch(() => {
        if (options.callback && typeof options.callback === "function") {
          if (options.failedResponse) {
            options.callback(options.failedResponse);
          } else {
            options.callback({
              errorMessage: "An error occurred sending the request"
            });
          }
        }
      });
  }
};

export const SSO_LOGIN = options => {
  SSO_API.POST("sso.login", merge(options, DEFAULT_HEADERS()));
};

export const SSO_REGISTER = options => {
  SSO_API.POST("sso.register", merge(options, DEFAULT_HEADERS()));
};

export const SSO_SMS_REGISTER = options => {
  SSO_API.POST("sso.smsregister", options);
};

export const SSO_RESEND_EMAIL = options => {
  SSO_API.POST("sso.resendEmail", merge(options, DEFAULT_HEADERS()));
};

export const SSO_VERIFY_EMAIL = options => {
  SSO_API.POST("sso.verifyEmail", options);
};

export const SSO_EMAIL_VERIFICATION = options => {
  SSO_API.POST("profile.emailVerification", options);
};

export const SSO_RESEND_CODE = options => {
  SSO_API.POST("sso.resendCode", options);
};

export const SSO_VERIFY_CODE = options => {
  SSO_API.POST("sso.verifyCode", options);
};

export const SUGGEST_KN = options => {
  SSO_API.POST("sso.suggestKapamilyaName", options);
};

export const SSO_FORGOT_KAPAMILYANAME = options => {
  SSO_API.POST("sso.forgotKapamilyaName", options);
};

export const SSO_SEND_PASSWORD_RESET = options => {
  var siteURL =
    store.state.view === "fullscreen"
      ? window.location.origin
      : store.state.siteURL;

  SSO_API.POST(
    "sso.sendPasswordReset",
    merge(options, DEFAULT_HEADERS(), {
      data: { siteURL }
    })
  );
};

export const SSO_RESEND_PASSWORD_RESET = options => {
  SSO_API.POST("sso.resendPasswordReset", merge(options, DEFAULT_HEADERS()));
};

export const SSO_RESET_PASSWORD = options => {
  SSO_API.POST("sso.resetPassword", options);
};

export const SSO_SOCIAL_LOGIN = options => {
  SSO_API.POST("sso.socialLogin", merge(options, DEFAULT_HEADERS()));
};

export const SSO_LINK_ACCOUNTS = options => {
  SSO_API.POST("sso.linkAccounts", merge(options, DEFAULT_HEADERS()));
};

export const SSO_PROFILE_COMPLETION = options => {
  SSO_API.POST("sso.profileCompletion", merge(options, DEFAULT_HEADERS()));
};

export const SSO_MOBILE_LOGIN = options => {
  SSO_API.POST("sso.mobileLogin", options);
};

export const SSO_NAME_COMPLETION = options => {
  SSO_API.POST("sso.nameCompletion", options);
};

export const SSO_SENDVERIF_CODE = options => {
  SSO_API.POST("sso.sendVerifCode", options);
};

export const SSO_ACCOUNT_INFO = options => {
  SSO_API.GET("sso.accountInfo", options);
};

export const SSO_CHURN_MOBILE = options => {
  SSO_API.POST("sso.churnMobile", options);
};

export const SSO_CREATE_PROFILECOMPLETIONTOKEN = options => {
  SSO_API.POST("sso.createProfileCompletionToken", options);
};

export const SSO_NOTIFY_LOGIN = options => {
  SSO_API.POST("sso.notifyLogin", options);
};

export const SSO_PRIVACY_ACCEPT = options => {
  SSO_API.PUT("gdpr.acceptPrivacy", options);
};

export const SSO_TERMS_ACCEPT = options => {
  SSO_API.PUT("gdpr.acceptTerms", options);
};

export const SSO_MARKETINGCONSENT_ACCEPT = options => {
  SSO_API.PUT("gdpr.acceptMarketingConsent", options);
};

import {SSO_PRIVACY_ACCEPT, SSO_TERMS_ACCEPT, SSO_MARKETINGCONSENT_ACCEPT } from '../services/index.js';

export function set_PrivactTermsConsent(_uid, _xparameter) {

    //set store privacy
    SSO_PRIVACY_ACCEPT({
        data: {
            versionTitle: _xparameter.privacyVersionTitle,
            uid: _uid,
            accepted: true
        }
    });

    //set store terms
    SSO_TERMS_ACCEPT({
            data: {
                versionTitle: _xparameter.termsVersionTitle,
                uid: _uid,
                accepted: true
            }
    });

    //set store marketing consent
    if(_xparameter.countryCode != 'PH'){
        SSO_MARKETINGCONSENT_ACCEPT({
                data: {
                    uid: _uid,
                    isAccepted: _xparameter.consent,
                    versionTitle: _xparameter.MCVersionTitle
                }
        });
    }
}
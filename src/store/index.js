/* eslint-disable */

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { getCookie } from 'tiny-cookie'
import { find, remove, cloneDeep } from 'lodash'

import getCountries from '@/services/getCountries'
import profileCompletion from './modules/profile-completion'
import { getHidePages, HidePage_LocalStorage } from '@/helpers/getHidePages'

Vue.use(Vuex);

const
    SET_VIEW = 'SET_VIEW',
    SET_ERROR = 'SET_ERROR',
    SET_CONFIG = 'SET_CONFIG',
    SET_VERIFTYPE = 'SET_VERIFMODE',
    SET_COUNTRIES = 'SET_COUNTRIES',
    SET_GIGYA_LOADED = 'SET_GIGYA_LOADED',
    SET_LOGGEDIN_USER = 'SET_LOGGEDIN_USER',
    
    INITIALIZE = 'INITIALIZE',
    GET_LOGGEDIN_USER = 'GET_LOGGEDIN_USER',
    
    HIDE_PAGE = 'HIDE_PAGE',
    HIDE_PAGES = 'HIDE_PAGES',
    GIGYA_LOADED = 'GIGYA_LOADED',
    ADD_LISTENERS = 'ADD_LISTENERS',
    INVOKE_LISTENERS = 'INVOKE_LISTENERS',
    
    KA_SITE_SOURCE = 'KA_SITE_SOURCE';

const getInfo = (response) => {
    return {
        UID: response.UID,
        UIDSignature: response.UIDSignature,
        created: response.created,
        isActive: response.isActive,
        isVerified: response.isVerified,
        isRegistered: response.isRegistered,
        lastLogin: response.lastLogin,
        lastUpdated: response.lastUpdated,
        loginProvider: response.loginProvider,
        oldestDataUpdated: response.oldestDataUpdated,
        registered: response.registered,
        socialProviders: response.socialProviders,
        loginIDs: cloneDeep(response.loginIDs),
        emails: cloneDeep(response.emails),
        profile: cloneDeep(response.profile),
        data: cloneDeep(response.data)
    };
}

export default new Vuex.Store({
    modules: { profileCompletion },
    
    state: {
        view: null,
        error: null,
        ocpKey: null,
        apiKey: null,
        appName: null,
        verifType: '',
        countries: [],
        ipCountry: null,
        hiddenPages: [],
        loggedInUser: null,
        isGigyaLoaded: false,
        siteURL: window.location.origin,
        ssoListeners: {
            onLogin: [],
            onLogout: [],
            onGetUserInfo: []
        },
        ka_sitesource : null
    },

    getters: {
        verifType: (state) => state.verifType.toUpperCase(),

        backPage: (state) => {
            const route = state.route;
            const backPage = route.meta.previousRoute || route.query.from;
            
            if (state.view === 'fullscreen' && backPage === '/default') {
                return null;
            }

            return backPage;
        },

        routeQuery: (state) => state.route.query
    },

    mutations: {
        [SET_VIEW](state, view) {
            state.view = view;
        },

        [SET_VERIFTYPE](state, verifType) {
            state.verifType = verifType;
        },

        [SET_CONFIG](state, data) {
            state.error = null;
            state.ocpKey = data.ocpKey;
            state.apiKey = data.apiKey;
            state.siteURL = data.siteURL;
            state.appName = data.appName;
            state.ipCountry = data.country;
        },

        [SET_LOGGEDIN_USER](state, user) {
            state.loggedInUser = user;
        },

        [SET_ERROR](state, error) {
            state.error = error;
        },

        [SET_GIGYA_LOADED](state) {
            state.isGigyaLoaded = true;
        },

        [HIDE_PAGES](state, pages) {
            state.hiddenPages = pages;
        },

        [SET_COUNTRIES](state, countries) {
            state.countries = countries;
        },

        [ADD_LISTENERS](state, { event, callback }) {
            if (state.ssoListeners[event]) {
                state.ssoListeners[event].push(callback);
            }
        },

        [KA_SITE_SOURCE](state, ka_site_source) {
            state.ka_sitesource = ka_site_source;
        },
    },

    actions: {
        [INITIALIZE]: ({ commit, state }, { ocpKey, view, verifType }) => {
            return new Promise((resolve, reject) => {
                const siteURL = getCookie('app_siteurl') || state.siteURL;
                
                let headers = { headers: { 'Ocp-Apim-Subscription-Key': ocpKey } };

                axios.get(`${process.env.VUE_APP_API_MNGR}/sso/api/apikey?ocpKey=${ocpKey}&siteUrl=${siteURL}`, headers)
                    .then(response => {

                        const kaSiteSource = getCookie('.ka_sitesource');
                        
                        commit(KA_SITE_SOURCE, {
                            ka_sitesource: decodeURIComponent(kaSiteSource)
                        });

                        axios.defaults.data = {};
                        axios.defaults.data['siteURL'] = siteURL;
                        axios.defaults.headers.common['Ocp-Apim-Subscription-Key'] = ocpKey;
                        
                        const app = response.data;
                        
                        commit(SET_CONFIG, {
                            ocpKey: ocpKey,
                            siteURL: siteURL,
                            apiKey: app.apiKey,
                            appName: app.appName,
                            country: app.country
                        });

                        resolve(app.apiKey);
                    }).catch(err => {
                        if (err.response) {
                            const data = err.response.data;
                            commit(SET_ERROR, data)
                            reject(data.errorDetails);
                        } else {
                            commit(SET_ERROR, { 
                                errorMessage: 'LOADING FAILED!',
                                errorDetails: 'An error occurred initiating <strong class="sso-d-block">Kapamilya Accounts Single Sign-on.</strong>'
                            });
                        }
                    });
                
                commit(SET_VIEW, view);
                commit(SET_VERIFTYPE, verifType);
                commit(HIDE_PAGES, getHidePages());
            });
        },

        [HIDE_PAGE]: ({ commit }, { notShow, page }) => {
            return new Promise((resolve) => {
                let pages = getHidePages();
                if (!pages) {
                    pages = [];
                }
                
                const _page = find(pages, function(cp) {
                    return cp.from === page.from;
                });

                if (notShow) {
                    if (!_page) {
                        pages.push(page);
                    }
                } else {
                    remove(pages, _page)
                }

                localStorage.setItem(
                    HidePage_LocalStorage,
                    JSON.stringify(pages)
                );
                    
                commit(HIDE_PAGES, pages);
                
                resolve();
            })
        },

        getCountries: ({ commit }) => {
            getCountries(countries => {
                commit(SET_COUNTRIES, countries);
            });
        },

        [GIGYA_LOADED]: ({ commit, dispatch }, gigya) => {
            function onLoginHandler() {
                gigya.setSSOToken({ redirectURL:  window.location.href });
            }

            gigya.socialize.addEventHandlers({
                onLogin: onLoginHandler
            });

            gigya.accounts.addEventHandlers({
                onLogin: onLoginHandler,
                onLogout: (e) => {
                    // uninav js
                    const uninav = window.uninav;
                    if (uninav && uninav._setLoginState) {
                        uninav._setLoginState({ state: 'FAIL' });
                    }

                    dispatch(INVOKE_LISTENERS, {
                        eventName: 'onLogout',
                        payload: e
                    });
                }
            });

            dispatch(GET_LOGGEDIN_USER, gigya).then(() => {
                commit(SET_GIGYA_LOADED);
            });
        },
        
        [GET_LOGGEDIN_USER]: ({ commit, dispatch }, gigya) => {
            return new Promise((resolve) => {
                gigya.accounts.getAccountInfo({
                    include: 'loginIDs, emails,  profile, data',
                    callback: function(response) {
                        resolve(response);
                        
                        const uninav = window.uninav;
                        if (uninav && uninav._setLoginState) {
                            uninav._setLoginState(response);
                        }

                        if (response.status === 'OK') {
                            dispatch(INVOKE_LISTENERS, {
                                eventName: 'onGetUserInfo',
                                payload: getInfo(response)
                            });
    
                            commit(SET_LOGGEDIN_USER, response);
                        } 
                    }
                });
            });
        },
        
        [INVOKE_LISTENERS]: ({ state }, { eventName, payload }) => {
            const eventListeners = state.ssoListeners[eventName];
            if (eventListeners && eventListeners.length > 0) {
                for (let i = 0; i < eventListeners.length; i++) {
                    const eventListener = eventListeners[i];
                    if (typeof eventListener === 'function') {
                        eventListener(cloneDeep(payload));
                    }
                }
            }
        } 
    }
});
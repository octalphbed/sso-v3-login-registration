const STORAGE_NAMESPACE = 'ssokapamilya:profile-completion:userdata'

export default {
    namespaced: true,

    state: {
        userData: JSON.parse(sessionStorage.getItem(STORAGE_NAMESPACE) || '{}')
    },

    getters: {
        UID (state) {
            return (state.userData || {}).UID
        },

        data (state) {
            return (state.userData || {}).data
        },
        
        profile (state) {
            return (state.userData || {}).profile
        },

        username (state, getters) {
            if (!state.userData || typeof state.userData === 'undefined')
                return null
            
            const loginIDs = state.userData.loginIDs
            if (!loginIDs || typeof loginIDs === 'undefined')
                return null

            return loginIDs.username || getters.profile.username
        },

        hasPassword(state) {
            if (!state.userData || typeof state.userData === 'undefined')
                return false
            
            const password = state.userData.password
            return (password && typeof password === 'string')
        },

        isRegistered(state) {
            if (!state.userData || typeof state.userData === 'undefined')
                return false
            
            return state.userData.isRegistered
        },

        token(state, getters, rootState) {
            return rootState.route.query.profileCompletionToken;
        },

        facebook(state) {
            return (state.userData || {}).facebook;
        }
    },
    
    mutations: {
        setUserData (state, payload) {
            state.userData = payload
        }
    },
    
    actions: {
        setProfileCompletion: ({ commit }, data) => {
            return new Promise((resolve) => {
                sessionStorage.setItem(STORAGE_NAMESPACE, 
                    JSON.stringify(data))

                commit('setUserData', data)
                resolve()
            })
        },

        clearProfileCompletion: ({ commit }) => {
            return new Promise((resolve) => {
                sessionStorage.removeItem(STORAGE_NAMESPACE)
                commit('setUserData', null)
                resolve()
            })
        }
    }
}
import axios from "axios";
import { debounce } from "lodash";
import { emailOrMobile, isNumeric } from "./emailOrMobile";
import { req, withParams } from "vuelidate/lib/validators/common";

const checkAvailable = debounce((api, callback) => {
	axios.get(api).then(response => {
		callback(response.data);
	});
}, 3000);

export const emAvailable = prop =>
	withParams({ type: "available", prop }, value => {
		if (!req(value) || !emailOrMobile(prop)(value)) return true;
		
		const URL_NUMBER = process.env.NODE_ENV == 'production'
			? '/sso/api/sso.isNumberAvailable'
			: '/api/sso.isNumberAvailable';
			
		const URL_EMAIL = process.env.NODE_ENV == 'production'
			? '/sso/api/sso.isEmailAvailable'
			: '/api/sso.isEmailAvailable';

		return new Promise(resolve => {
			const api = isNumeric(value)
				? `${URL_NUMBER}?mobileNumber=${value}`
				: `${URL_EMAIL}?email=${value}`;

			checkAvailable(api, callback => {
				resolve(callback.available);
			});
		});
	});

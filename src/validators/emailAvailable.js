import axios from 'axios';
import { debounce } from 'lodash';
import { isEmail } from './isEmail';
import { req, withParams } from 'vuelidate/lib/validators/common';

const checkAvailable = debounce((api, callback) => {
	axios.get(api).then(response => {
		callback(response.data);
	});
}, 3000);

export const emailAvailable = prop =>
	withParams({ type: 'available', prop }, (value, vm) => {
		if (!req(value) || !isEmail(false)(value)) return true;

		const URL = process.env.NODE_ENV == 'production'
			? '/sso/api/sso.isEmailAvailable'
			: '/api/sso.isEmailAvailable';

		return new Promise(resolve => {
			const api = `${URL}?email=${value}`;
			checkAvailable(api, callback => {
				vm.emailError = callback.message;
				resolve(callback.available);
			});
		});
	});

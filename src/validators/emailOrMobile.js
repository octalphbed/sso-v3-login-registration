/* eslint-disable */

import { helpers } from "vuelidate/lib/validators";
import { MOBILE_REGEX, EMAIL_REGEX } from '@/helpers/regex';

export const isNumeric = x => {
	return (typeof x === "number" || typeof x === "string") && !isNaN(Number(x));
};

export const emailOrMobile = prop =>
	helpers.withParams({ type: "email-or-mobile" }, value => {
		if (helpers.req(value)) {
			if (isNumeric(value) && prop.includes("mobile")) {
				return MOBILE_REGEX.test(value);
			}

			if (prop.includes("email")) {
				return EMAIL_REGEX.test(value);
			}
		}

		return true;
	});

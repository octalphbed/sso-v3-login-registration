import { isEmail } from './isEmail';
import { isMobile } from './isMobile';
import { isUsername } from './isUsername';
import { emAvailable } from './emAvailable';
import { emailAvailable } from './emailAvailable';
import { mobileAvailable } from './mobileAvailable';
import { unAvailable } from './unAvailable';
import { emailOrMobile, isNumeric } from './emailOrMobile.js';
import { required, maxLength, minLength, sameAs, helpers } from 'vuelidate/lib/validators';

export const validators = {
	isEmail,
	isMobile,
	isNumeric,
	isUsername,
	emAvailable,
	emailAvailable,
	mobileAvailable,
	unAvailable,
	emailOrMobile,

	required,
	maxLength,
	minLength,
	sameAs,
	helpers
}

export {
	isEmail,
	isMobile,
	isNumeric,
	isUsername,
	emAvailable,
	emailAvailable,
	mobileAvailable,
	unAvailable,
	emailOrMobile
}
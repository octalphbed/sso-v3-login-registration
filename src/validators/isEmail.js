/* eslint-disable */

import { req, withParams } from "vuelidate/lib/validators/common";

const EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

export const isEmail = (reverse = false) =>
  withParams({ type: "isEmail", reverse }, value => {
    if (!req(value)) return true;

    return reverse ? !EMAIL_REGEX.test(value) : EMAIL_REGEX.test(value);
  });

/* eslint-disable */

import { req, withParams } from "vuelidate/lib/validators/common";

const MOBILE_REGEX = /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;

export const isMobile = (reverse = false) =>
  withParams({ type: "isMobile", reverse }, value => {
    if (!req(value)) return true;

    return reverse ? !MOBILE_REGEX.test(value) : MOBILE_REGEX.test(value);
  });

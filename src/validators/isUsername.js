/* eslint-disable */

import { req, withParams } from "vuelidate/lib/validators/common";
import {  } from 'vuelidate/lib/validators';

const NUMERIC_REGEX = /^[+\d@.-_]*$/;
const USERNAME_REGEX = /^([\da-zA-Z]+[.@-_]+[\da-zA-Z]*|[\d]*[a-zA-Z]+|[@][\da-zA-Z]*|[\d]*\w+)$/;


let notAllowedChars = new RegExp('[ `~!#$%^&*()+ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑñÕãõÄËÏÖÜŸäëïöüŸ¡¿çÇŒœßØøÅåÆæÞþÐð{}\[;:"|\\\]<>,/]', 'g');

export const isUsername = (reverse = false) =>
  withParams({ type: 'isUsername', reverse }, value => {
    if (!req(value)) return true;

    if (!/[a-zA-Z]/.test(value)) return false;
    
    if (NUMERIC_REGEX.test(value))
      return false;
    
    let isValid = notAllowedChars.exec(value) ? false : true;
    return isValid; //reverse ? !USERNAME_REGEX.test(value) : USERNAME_REGEX.test(value);
  });

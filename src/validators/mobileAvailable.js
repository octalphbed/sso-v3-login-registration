import axios from "axios";
import { debounce } from "lodash";
// import { isMobile } from './isMobile';
import { req, withParams } from "vuelidate/lib/validators/common";

const checkAvailable = debounce((api, callback) => {
	axios.get(api).then(response => {
		callback(response.data);
	});
}, 3000);

export const mobileAvailable = prop =>
	withParams({ type: "available", prop }, value => {
		if (!req(value)) return true;

		const URL = process.env.NODE_ENV == 'production'
			? '/sso/api/sso.isNumberAvailable'
			: '/api/sso.isNumberAvailable';

		return new Promise(resolve => {
			const api = `${URL}?mobileNumber=${value}`;
			checkAvailable(api, callback => {
				resolve(callback.available);
			});
		});
	});

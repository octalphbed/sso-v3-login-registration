import axios from 'axios';
import { debounce } from 'lodash';
import { isEmail, isMobile, isUsername } from './index';
import { req, withParams, len } from 'vuelidate/lib/validators/common';

const checkAvailable = debounce((api, callback) => {
	axios.get(api).then(response => {
		callback(response.data);
	});
}, 3000);

export const unAvailable = (prop, selfUID) =>
	withParams({ type: 'available', prop }, value => {
		if (
			!req(value) ||
			!isEmail(true)(value) ||
			!isMobile(true)(value) ||
			!isUsername()(value) ||
			len(value) < 4 ||
			len(value) > 30
		) return true;

		return new Promise(resolve => {
			const URL = process.env.NODE_ENV == 'production'
				? '/sso/api/sso.isKapamilyaNameAvailable'
				: '/api/sso.isKapamilyaNameAvailable';

			const api = `${URL}?kapamilyaName=${value}&selfUID=${selfUID}`;

			checkAvailable(api, callback => {
				resolve(callback.available);
			});
		});
	});

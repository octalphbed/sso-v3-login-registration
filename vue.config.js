const zopfli = require("@gfx/zopfli");
const CompressionPlugin = require("compression-webpack-plugin");

// const createPages = function() {
// 	if (process.env.NODE_ENV === 'production') {
// 		return {
// 			'sso-app': 'src/main.js',
// 			'sso-plugin': 'src/plugin.js'
// 		};
// 	} else {
// 		return { 
// 			index: {
// 				entry: 'src/main.js',
// 				template: 'public/index.html'
// 			}
// 		};
// 	}
// }
 
module.exports = {

	// pages: createPages(),

	productionSourceMap: false,

	baseUrl: process.env.VUE_APP_BASE_URL,

	chainWebpack: config => {
		if (process.env.NODE_ENV === 'production') {

			config.optimization.delete('splitChunks');

			config.plugin('gzip').use(CompressionPlugin, [
				{
					compressionOptions: { numiterations: 15 },

					algorithm(input, compressionOptions, callback) {
						return zopfli.gzip(input, compressionOptions, callback);
					}
				}
			]);

			if(config.plugins.has('extract-css')) {
				const extractCSSPlugin = config.plugin('extract-css')
				extractCSSPlugin && extractCSSPlugin.tap(() => [{
					filename: 'css/sso-[name].css',
					chunkFilename: 'css/[name].[hash:8].css'
				}])
			}
		}
	},

	configureWebpack: {
        output: {
			filename: 'js/sso-[name].js',
			chunkFilename: 'js/[name].[hash:8].js'
        }
    }
};
